package com.ysh.studyjava.service;

import com.ysh.studyjava.model.ExampleItem;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service                     //서비스
public class ExampleService {    //
    private List<ExampleItem> settingDate() {
        List<ExampleItem> result = new LinkedList<>();  // 빈 상자(exampleitem)을 생성했다.

        ExampleItem item1 = new ExampleItem();    //item에 item1을 생성했다.
        item1.setName("홍길동");           // 이름을 생성
        item1.setBirthDay(LocalDate.of(2022,2,4)); //생일 생성
        result.add(item1);       //item1을 item을 넣었다.

        ExampleItem item2 = new ExampleItem();
        item2.setName("박경철");
        item2.setBirthDay(LocalDate.of(1999,2,4));
        result.add(item2);

        ExampleItem item3 = new ExampleItem();
        item3.setName("박철주");
        item3.setBirthDay(LocalDate.of(1985,3,1));
        result.add(item3);

        return result;   // 결과을
    }

    public  List<String>getForeachTest(){     //문자열을 빈상자을 만든다.
            List<ExampleItem> data = settingDate();  //변수 settingDate 을 EXampleItem에 저정한다.
            List<String> result = new LinkedList<>();  //

            data.forEach(item -> result .add(item.getName())) ;  //

            for (ExampleItem item : data){
                result.add(item.getName());

            }
            return result;
                 //예상결과물
    }

}
